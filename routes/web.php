<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\TransactionHeaderController;
use App\Http\Controllers\TransactionDetailController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/dashboard', function () {
    return redirect('/');
})->middleware(['auth']);

require __DIR__.'/auth.php';


Route::resource('products', ProductController::class);
Route::resource('transactionheaders', TransactionHeaderController::class);
Route::resource('transactiondetails', TransactionDetailController::class);
Route::resource('carts', CartController::class);

Route::get('/', [HomeController::class, 'index']);
Route::get('view-cart', [CartController::class, 'cart'])->name('cart');
Route::get('add-to-cart/{id}', [CartController::class, 'addToCart'])->name('add.to.cart');
Route::patch('update-cart', [CartController::class, 'update'])->name('update.cart');
Route::delete('remove-from-cart', [CartController::class, 'remove'])->name('remove.from.cart');
Route::post('checkout-cart', [CartController::class, 'checkoutCart'])->name('checkout.cart');

Route::get('transactiondetails/show/{document_number}', [TransactionDetailController::class, 'getByDocNum']);
