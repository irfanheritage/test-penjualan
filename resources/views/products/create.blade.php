@extends('layouts.app')
@section('title', 'Create New Product')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="float-right">
            <a class="btn btn-primary" href="{{ route('products.index') }}"><i class="material-icons">chevron_left</i> Back</a>
        </div>
    </div>
</div>

   

@if ($errors->any())
    <div class="alert alert-danger" role="alert">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



<div class="col-md-12">
    <div class="card ">
    <form action="{{ route('products.store') }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}
        <div class="card-header card-header-rose card-header-text">
          <div class="card-text">
            <h4 class="card-title">New Product Form</h4>
          </div>
        </div>
        <div class="card-body ">
            <div class="row">
                <label class="col-sm-2 col-form-label">Product Code</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                      <input type="text" class="form-control" name="product_code">
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 col-form-label">Product Name</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                      <input type="text" class="form-control" name="product_name">
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 col-form-label">Price</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                        <input type="text" class="form-control" name="price">
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 col-form-label">Currency</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                        <input type="text" class="form-control" name="currency">
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 col-form-label">Discount</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                        <input type="text" class="form-control" name="discount">
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 col-form-label">Dimension</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                        <input type="text" class="form-control" name="dimension">
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 col-form-label">Unit</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                        <input type="text" class="form-control" name="unit">
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer ">
            <button type="submit" class="btn btn-fill btn-primary">Submit</button>
        </div>
    </form>
    </div>
</div>

@endsection