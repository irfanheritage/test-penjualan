@extends('layouts.app')
@section('title', 'Show Product')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="float-right">
            <a class="btn btn-primary" href="{{ route('products.index') }}"><i class="material-icons">chevron_left</i> Back</a>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="card ">
        <div class="card-header card-header-rose card-header-text">
          <div class="card-text">
            <h4 class="card-title">@yield('title', 'Show Data') Form</h4>
          </div>
        </div>
        <div class="card-body ">
            <div class="row">
                <label class="col-sm-2 col-form-label">Product Code</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                      {{$product->product_code}}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 col-form-label">Product Name</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                      {{$product->product_name}}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 col-form-label">Price</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                      {{$product->price}}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 col-form-label">Currency</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                      {{$product->currency}}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 col-form-label">Discount</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                      {{$product->discount}}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 col-form-label">Dimension</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                      {{$product->dimension}}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 col-form-label">Unit</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                      {{$product->unit}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

