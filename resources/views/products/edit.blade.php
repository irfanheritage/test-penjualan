@extends('layouts.app')
@section('title', 'Edit Product')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="float-right">
            <a class="btn btn-primary" href="{{ route('products.index') }}"><i class="material-icons">chevron_left</i> Back</a>
        </div>
    </div>
</div>

   

@if ($errors->any())
    <div class="alert alert-danger" role="alert">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



<div class="col-md-12">
    <div class="card ">
    <form action="{{ route('products.update', $product->id) }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}
        {{method_field('PUT')}}
        <div class="card-header card-header-rose card-header-text">
          <div class="card-text">
            <h4 class="card-title">Edit Product Form</h4>
          </div>
        </div>
        <div class="card-body ">
            <div class="row">
                <label class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                      <input type="text" class="form-control" name="name" value="{{$product->name}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 col-form-label">Join Date</label>
                <div class="col-sm-10">
                    <div class="form-group bmd-form-group">
                        <input type="text" class="form-control" name="detail" value="{{$product->detail}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer ">
            <button type="submit" class="btn btn-fill btn-primary">Submit</button>
        </div>
    </form>
    </div>
</div>

@endsection
