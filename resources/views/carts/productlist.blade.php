@extends('layouts.app')

@section('title', 'Product List')

@section('content')

<div class="container-fluid">
  <div class="row">
  	<div class="col-md-12 ">
	    @if ($message = Session::get('success'))
        <div class="alert alert-success" role="alert">
            <p>{{ $message }}</p>
        </div>
    	@endif
  	</div>
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-success card-header-icon">
          <div class="card-icon">
            <i class="material-icons">face</i>
          </div>
          <h4 class="card-title">@yield('title')</h4>
        </div>
        <div class="card-body">
          <div class="toolbar">
            <!--        Here you can write extra buttons/actions for the toolbar              -->
          </div>
          <div class="material-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Currency</th>
                    <th>Discount</th>
                    <th>Dimension</th>
                    <th>Unit</th>
                    <th class="disabled-sorting text-right">&nbsp;</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Currency</th>
                    <th>Discount</th>
                    <th>Dimension</th>
                    <th>Unit</th>
                    <th class="text-right">&nbsp;</th>
                </tr>
              </tfoot>
              <tbody>
              	@foreach ($products as $product)
                <tr>
                  <td>{{$product->product_code}}</td>
                  <td>{{$product->product_name}}</td>
                  <td>{{$product->price}}</td>
                  <td>{{$product->currency}}</td>
                  <td>{{$product->discount}}</td>
                  <td>{{$product->dimension}}</td>
                  <td>{{$product->unit}}</td>
                  <td class="text-right">
                    <a href="{{route('products.show', $product->id)}}" class="btn btn-link btn-info btn-just-icon like"><i class="material-icons">search</i></a>
                    <a href="{{ route('add.to.cart', $product->id) }}" class="btn btn-info"><i class="material-icons">add_shopping_cart</i> Add to Cart</a>
                    <!-- <a href="#" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">remove_circle</i></a> -->
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <!-- end content-->
      </div>
      <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
  </div>
  <!-- end row -->
</div>

@endsection

@section('jspage')

<script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });
    });
</script>
@endsection