@extends('layouts.app')

@section('title', 'Cart')

@section('content')

<div class="container-fluid">
  <div class="row">
  	<div class="col-md-12 ">
	    @if ($message = Session::get('success'))
        <div class="alert alert-success" role="alert">
            <p>{{ $message }}</p>
        </div>
    	@endif
  	</div>
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-success card-header-icon">
          <div class="card-icon">
            <i class="material-icons">face</i>
          </div>
          <h4 class="card-title">@yield('title')</h4>
        </div>
        <div class="card-body">
          <div class="toolbar">
            <!--        Here you can write extra buttons/actions for the toolbar              -->
          </div>
          <div class="material-datatables">
            <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Sub Total</th>
                    <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody class="cart-data">
              @php $total = 0 @endphp
              @if(session('cart'))
                  @foreach(session('cart') as $id => $details)
                      @php $total += $details['price'] * $details['qty'] @endphp
                      <tr data-id="{{ $id }}">
                        <td><a href="{{route('products.show', $id)}}">{{$details['product_name']}}</a></td>
                        <td>{{$details['price']}}</td>
                        <td><input type="number" value="{{ $details['qty'] }}" class="form-control qty update-cart" /></td>
                        <td class="text-right">{{ $details['price'] * $details['qty'] }}</td>
                        <td class="text-right">
                        <button class="btn btn-danger btn-sm remove-from-cart">Remove</button>
                          <!-- <a href="#" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">remove_circle</i></a> -->
                        </td>
                      </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan="5">Cart Empty</td>
                  </tr>
                @endif
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="4" class="text-right">Total: {{$total}}</td>
                  <td class="text-right"><button class="btn btn-primary checkout-cart">Confirm</button></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <!-- end content-->
      </div>
      <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
  </div>
  <!-- end row -->
</div>

@endsection

@section('jspage')

<script>
  $(".update-cart").change(function (e) {
        e.preventDefault();
  
        var ele = $(this);
  
        $.ajax({
            url: "{{ route('update.cart') }}",
            method: "patch",
            data: {
                _token: '{{ csrf_token() }}', 
                id: ele.parents("tr").attr("data-id"), 
                qty: ele.parents("tr").find(".qty").val()
            },
            success: function (response) {
               window.location.reload();

            }
        });
    });
  
    $(".remove-from-cart").click(function (e) {
        e.preventDefault();
  
        var ele = $(this);
  
        if(confirm("Are you sure want to remove?")) {
            $.ajax({
                url: '{{ route('remove.from.cart') }}',
                method: "DELETE",
                data: {
                    _token: '{{ csrf_token() }}', 
                    id: ele.parents("tr").attr("data-id")
                },
                success: function (response) {
                    window.location.reload();
                }
            });
        }
    });

    $(".checkout-cart").click(function (e) {
        e.preventDefault();
        var arr = [];
        $('.cart-data tr').each(function(i){
          var item = [];
          var id = $(this).attr("data-id");
          var qty = $(this).find(".qty").val();
          // console.log($id);
          item = [id, qty];
          arr.push(item);
        });
        var ele = $(this);
        if(confirm("Are you sure?")) {
          $.ajax({
              url: "{{ route('checkout.cart') }}",
              method: "post",
              data: {
                  _token: '{{ csrf_token() }}', 
                  data: arr,
                  total: "{{$total}}",
              },
              success: function (response) {
                 window.location.reload();

              }
          });
        }
    });
</script>
@endsection
