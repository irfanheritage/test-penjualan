@extends('layouts.app')

@section('title', 'Products')

@section('content')

<div class="container-fluid">
  <div class="row">
  	<div class="col-md-12 ">
	    @if ($message = Session::get('success'))
        <div class="alert alert-success" role="alert">
            <p>{{ $message }}</p>
        </div>
    	@endif
  	</div>
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-success card-header-icon">
          <div class="card-icon">
            <i class="material-icons">face</i>
          </div>
          <h4 class="card-title">@yield('title') Data</h4>
        </div>
        <div class="card-body">
          <div class="toolbar">
            <!--        Here you can write extra buttons/actions for the toolbar              -->
          </div>
          <div class="material-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
              <thead>
                <tr>
                    <th>Document Code</th>
                    <th>Document Number</th>
                    <th>Product Code</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>Sub Total</th>
                    <th>Currency</th>
                    <th class="disabled-sorting text-right">&nbsp;</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                <th>Document Code</th>
                    <th>Document Number</th>
                    <th>Product Code</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>Sub Total</th>
                    <th>Currency</th>
                    <th class="text-right">&nbsp;</th>
                </tr>
              </tfoot>
              <tbody>
              	@foreach ($products as $product)
                <tr>
                  <td>{{$product->document_code}}</td>
                  <td>{{$product->document_number}}</td>
                  <td>{{$product->product_code}}</td>
                  <td>{{$product->price}}</td>
                  <td>{{$product->quantity}}</td>
                  <td>{{$product->unit}}</td>
                  <td>{{$product->sub_total}}</td>
                  <td>{{$product->currency}}</td>
                  <td class="text-right">
                    <!-- <a href="#" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">remove_circle</i></a> -->
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <!-- end content-->
      </div>
      <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
  </div>
  <!-- end row -->
</div>

@endsection

@section('jspage')

<script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });
    });
</script>
@endsection