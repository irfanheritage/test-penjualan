@extends('layouts.page')

@section('content')
    <div class="wrapper sign-wrapper " style="background: url('{{ asset('img/bg.jpg') }}') no-repeat; background-size: cover;">
        <div class="page-panel">
            <div class="content">
                <div class="container signin">
                    <div class="row justify-content-md-center">
                        <div class="col-md-6">
                            <div class="card ">
                                <div class="card-header card-header-rose card-header-icon">
                                  <div class="card-icon">
                                    <i class="material-icons">person</i>
                                  </div>
                                  <h4 class="card-title">Sign In</h4>
                                </div>
                                <div class="card-body ">
                                  <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                    @csrf

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                            <div class="col-md-12">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="col-md-4 control-label">Password</label>

                                            <div class="col-md-12">
                                                <input id="password" type="password" class="form-control" name="password" required>

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                Don't have an account? <a href="/register">Register</a>
                                            </div>
                                        </div>

                                        <!-- <div class="form-group">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary">
                                                    Login
                                                </button>

                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    Forgot Your Password?
                                                </a>
                                            </div>
                                        </div> -->
                                </div>
                                <div class="card-footer ">
                                  <div class="row">
                                    <div class="col-md-9">
                                      <button type="submit" class="btn btn-fill btn-rose">Sign in</button>
                                    </div>
                                  </div>
                                </div>
                                    </form>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

