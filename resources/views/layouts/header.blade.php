<div class="container-fluid">
    <div class="navbar-wrapper">
        <a class="navbar-brand" href="#pablo">@yield('title', 'No Title')</a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="sr-only">Toggle navigation</span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end">
        
        <ul class="navbar-nav">
            <!-- <li class="nav-item">
            <a class="nav-link" href="#pablo">
                <i class="material-icons">dashboard</i>
                <p class="d-lg-none d-md-block">
                Stats
                </p>
            </a>
            </li> -->
            <li class="nav-item dropdown">
            <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="material-icons">shopping_cart</i>
                <span class="notification">{{ count((array) session('cart')) }}</span>
                <p class="d-lg-none d-md-block">
                Shopping cart
                </p>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                @php $total = 0 @endphp
                @foreach((array) session('cart') as $id => $details)
                    @php $total += $details['price'] * $details['qty'] @endphp
                @endforeach
                <p class="cart-header">Your Cart</p>
                @if(session('cart'))
                    <ul style="margin: 0; padding: 0;">
                    @foreach(session('cart') as $id => $details)
                        <li class="cart-list dropdown-item">{{ $details['product_name']}} {{ $details['price'] }} <span class="count">Qty:{{$details['qty']}}</li>
                    @endforeach
                    </ul>
                <p class="item-total text-right">Total: {{ $total }}</p>
                <a class="btn btn-primary btn-full text-center" href="{{ route('cart') }}">View Cart</a>
                @else
                <a class="dropdown-item" href="#">No Product in Cart</a>
                @endif
            </div>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="material-icons">person</i>
                <p class="d-lg-none d-md-block">
                Account
                </p>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                <a class="dropdown-item" href="#">Profile</a>
                <a class="dropdown-item" href="#">Settings</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    Log Out
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
            </li>
        </ul>
    </div>
</div>