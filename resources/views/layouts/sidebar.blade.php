<div class="sidebar" data-color="azure" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <div class="logo">
        <a href="/dashboard" class="simple-text logo-normal text-center">
            POS
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item {{url()->current() == url('dashboard') ? 'active': ''}} ">
            <a class="nav-link" href="{{ url('dashboard')}}">
                <i class="material-icons">dashboard</i>
                <p>Dashboard</p>
            </a>
            </li>
            <li class="nav-item {{url()->current() == url('carts') ? 'active': ''}}">
            <a class="nav-link" href="{{url('carts')}}">
                <i class="material-icons">storefront</i>
                <p>Shopping</p>
            </a>
            </li>
            
            <li class="nav-item ">
                <a class="nav-link collapsed" data-toggle="collapse" href="#pagesExamples" aria-expanded="false">
                  <i class="material-icons">apps</i>
                  <p> Master Data
                    <b class="caret"></b>
                  </p>
                </a>
                <div class="collapse" id="pagesExamples" style="">
                  <ul class="nav">
                    <li class="nav-item ">
                      <a class="nav-link" href="{{url('transactionheaders')}}">
                        <span class="sidebar-mini"> T </span>
                        <span class="sidebar-normal"> Transaction Reports </span>
                      </a>
                    </li>
                    <li class="nav-item ">
                      <a class="nav-link" href="{{url('products')}}">
                        <span class="sidebar-mini"> P </span>
                        <span class="sidebar-normal"> Products </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
        </ul>
    </div>  
</div>