<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdditionalTransactionDetailColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_details', function (Blueprint $table) {
            //
            $table->string('unit');
            $table->decimal('sub_total');
            $table->string('currency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transcation_details', function (Blueprint $table) {
            //
            $table->dropColumn('unit');
            $table->dropColumn('sub_total');
            $table->dropColumn('currency');
        });
    }
}
