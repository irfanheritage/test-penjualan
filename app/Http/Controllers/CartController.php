<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\TransactionHeader;
use App\Models\TransactionDetail;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

        $products = Product::all();
        return view('carts.productlist', compact('products'));
    }

    public function cart(){
        return view('carts.cart');
    }

    public function addToCart($id){
        $product = Product::findOrFail($id);

        $cart = session()->get('cart', []);
        // echo "<pre>";print_r($cart);exit();
        if(isset($cart[$id])){
            $cart[$id]['qty']++;
        }else{
            $cart[$id] = [
                "product_name" => $product->product_name,
                "qty" => 1,
                "price" => $product->price,
            ];
        }
        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to Cart successfully');
    }

    public function update(Request $request)
    {
        
        if($request->id && $request->qty){
            $cart = session()->get('cart');
            $cart[$request->id]["qty"] = $request->qty;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function checkoutCart(Request $request){
        if($request->data){
            $head = TransactionHeader::latest()->first();
            if($head){
                $docnum = intval($head->document_number);
                $newdocnum = sprintf('%08d', $docnum + 1);
            }else{
                $newdocnum = sprintf('%08d', 1);
            }
            TransactionHeader::create([
                'document_code' => 'TRX',
                'document_number' => $newdocnum,
                'user' => Auth::user()->email,
                'total' => $request->total,
                'date' => date('Y-m-d H:i:s')
            ]);
            $data = $request->data;
            foreach($data as $d){
                $product = Product::findOrFail($d[0]);
                TransactionDetail::create([
                    'document_code' => 'TRX',
                    'document_number' => $newdocnum,
                    'product_code' => $product->product_code,
                    'price' => $product->price,
                    'quantity' => $d[1],
                    'unit' => $product->unit,
                    'sub_total' => $product->price * $d[1],
                    'currency' => $product->currency
                ]);

                if($d[0]) {
                    $cart = session()->get('cart');
                    if(isset($cart[$d[0]])) {
                        unset($cart[$d[0]]);
                        session()->put('cart', $cart);
                    }
                }
            }
            session()->flash('success', 'Product removed successfully');

            
        }
    }

}
